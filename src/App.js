import React from 'react';
import style from './App.module.css';
import Header from "./components/Header/Header";
import Nav from "./components/Nav/nav";
import Profile from "./components/Profile/Profile";
import Dialogs from "./components/Dialoges/Dialogs";
import {BrowserRouter, Route} from "react-router-dom";
import Friends from "./components/Friends/Friends";
import Music from "./components/Music/Music";
import Games from "./components/Games/Games";


const App = (props) => {
    return (
        <BrowserRouter>
            <div className={style.container}>
                <Header/>
                <div className={style.container}>
                    <div className={style.main}>
                        <Nav state={props.state.sidebarPage}/>
                        <div className={style.main__col}>
                            <Route path='/dialogs'
                                   render={() => <Dialogs state={props.state.dialogsPage} dispatch={props.dispatch}/>}/>
                            <Route path='/profile'
                                   render={() => <Profile state={props.state.profilePage}
                                                          dispatch={props.dispatch}/>}/>
                            <Route path='/friends' render={() => <Friends/>}/>
                            <Route path='/music' render={() => <Music/>}/>
                            <Route path='/games' render={() => <Games/>}/>
                        </div>
                    </div>
                </div>
            </div>
        </BrowserRouter>
    );

}

export default App;
