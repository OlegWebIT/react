let initialState = {
        dialogs: [
            {id: '1', name: 'Dmitriy'},
            {id: '2', name: 'Sasha'},
            {id: '3', name: 'Dasha'},
            {id: '4', name: 'Masha'},
            {id: '5', name: 'Maks'}
        ],
        messages: [
            {text: 'How are you?'},
            {text: 'Who are you?'},
            {text: 'What is your name?'},
            {text: 'I am really glad!'},
            {text: 'How old are you?'}
    ],
}

const dialogsReducer = (state = initialState, action) => {

        if  (action.type === 'ADD-MESSAGE') {
            debugger;
            let newMessage = {
                text: action.messageText
            };
            state.messages.push(newMessage);
        }
    return state
}

export default dialogsReducer;
