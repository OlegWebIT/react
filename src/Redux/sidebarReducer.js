let initialState = {
    sidebarNames: [
        {name: 'Alex'},
        {name: 'Max'},
        {name: 'Oleg'},
    ]
}

const sidebarReducer = (state = initialState) => {
    return state
}

export default sidebarReducer;