import dialogsReducer from "./dialogsReducer";
import profileReducer from "./profileReducer";
import sidebarReducer from "./sidebarReducer";

let store = {
    _state: {
        profilePage: {
            posts: [
                {com: 'I really hype!'},
                {com: 'I in the high!'},
                {com: 'Yuuummmmyyy!!'},
                {com: 'Babadash!'},
                {com: 'hi!'}
            ],

        },
        dialogsPage: {
            dialogs: [
                {id: '1', name: 'Dmitriy'},
                {id: '2', name: 'Sasha'},
                {id: '3', name: 'Dasha'},
                {id: '4', name: 'Masha'},
                {id: '5', name: 'Maks'}
            ],
            messages: [
                {text: 'How are you?'},
                {text: 'Who are you?'},
                {text: 'What is your name?'},
                {text: 'I am really glad!'},
                {text: 'How old are you?'}
            ]
        },
        sidebarPage: {
            sidebarNames: [
                {name: 'Alex'},
                {name: 'Max'},
                {name: 'Oleg'},
            ]
        },
    },

    dispatch(action) {
        this._state.dialogsPage = dialogsReducer(this._state.dialogsPage, action);
        this._state.profilePage = profileReducer(this._state.profilePage, action);
        this._state.sidebarPage = sidebarReducer(this._state.sidebarPage, action);
        this._state.renderPage(this._state)
        },

    subscribe(observer) {
        this._state.renderPage = observer;
    },

}

export default store;