import React from "react";
import style from "./createMessage.module.css"

const CreateMessage = (props) => {
    debugger;
    let newMessage = React.createRef();
    let addMessage = () => {
        let messageText = newMessage.current.value;
        props.dispatch({type: 'ADD-MESSAGE', messageText});
        newMessage.current.value = '';
    }

    return (
        <div>
            <div><input ref={newMessage} className={style.input_item} placeholder="write your message..."/></div>
           <div><button onClick={addMessage} className={style.button}>Send</button></div>
        </div>
    )
}
export default CreateMessage