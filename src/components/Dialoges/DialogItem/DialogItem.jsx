import React from "react";
import s from './../Dialogs.module.css'
import {NavLink} from "react-router-dom";



const DialogItem = (props) => {
    return (
        <div className={s.flex}>
            <img src='https://i.pinimg.com/originals/04/a8/73/04a87347b071ec062a586e02c23f6221.png'/>
        <NavLink className={s.a} to={"/dialogs/" + props.id}>
            <li>{props.name}</li>
        </NavLink>
        </div>
    )
}

export default DialogItem