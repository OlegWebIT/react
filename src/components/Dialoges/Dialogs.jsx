import React from "react";
import s from './Dialogs.module.css'
import Message from "./MessageItem/MessageItem";
import DialogItem from "./DialogItem/DialogItem";
import CreateMessage from "./CreateMessage/createMessage";

const Dialogs = (props) => {

    let DialogsEl = props.state.dialogs.map(d => <DialogItem name={d.name} id={d.id}/>)
    let MessageEl = props.state.messages.map(m => <Message text={m.text}/>)

    return (
        <div className={s.dialogs}>
            <div className={s.title}>Dialogs</div>
            <div className={s.block}>
                <div className={s.block__item}>
                    <ul>
                        {DialogsEl}
                    </ul>
                </div>
                <div className={s.block__item}>
                    {MessageEl}
                </div>
            </div>
            <CreateMessage dispatch={props.dispatch}/>
        </div>
    )
}

export default Dialogs