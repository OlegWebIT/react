import React from 'react';
import Head from './Header.module.css';

const Header = () => {
    return (
            <header className={Head.header}>
                <img src='https://www.freelogodesign.org/Content/img/logo-samples/flooop.png'/>
            </header>
    );
}

export default Header