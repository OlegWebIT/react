import React from 'react';
import s from './nav.module.css'
// import Nav_item from "./item_nav/nav_item";
import {NavLink} from "react-router-dom";
import SideBar from "./sideBar/sideBar";

const Nav = (props) => {
    return (
            <div className={s.body__main__links}>
                <div className={s.main__links}>
                    <p><NavLink to='/profile' ClassName={s.a} activeClassName={s.active}>Profile</NavLink></p>
                </div>
                <div className={s.main__links}>
                    <p><NavLink to='/dialogs' ClassName={s.a} activeClassName={s.active}>Messages</NavLink></p>
                </div>
                <div className={s.main__links}>
                    <p><NavLink to='/friends' ClassName={s.a} activeClassName={s.active}>Friends</NavLink></p>
                    <SideBar sideBar={props.state.sidebarNames}/>
                </div>
                <div className={s.main__links}>
                    <p><NavLink to='/music' ClassName={s.a} activeClassName={s.active}>Music</NavLink></p>
                </div>
                <div className={s.main__links}>
                    <p><NavLink to='/games' ClassName={s.a} activeClassName={s.active}>Games</NavLink></p>
                </div>
            </div>);
            }

export default Nav