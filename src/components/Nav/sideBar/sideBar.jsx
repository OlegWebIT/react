import React from 'react';
import bar from './sideBar.module.css'
import SideBarItem from "./sideBar_item/sideBar_Item";


const SideBar = (props) => {
    let sideBarEl = props.sideBar.map (s => <SideBarItem name={s.name}/>)
    return (
        <div className={bar.sideBar}>
            {sideBarEl}
        </div>
    )}

export default SideBar