import React from 'react';
import bar from '.././sideBar.module.css'


const SideBarItem = (props) => {

    return (
            <div className={bar.sideBar}>
                <div className={bar.sideBar_col}>
                    <div className={bar.sideBar_img}>
                        <img src='https://i.pinimg.com/originals/04/a8/73/04a87347b071ec062a586e02c23f6221.png'/>
                    </div>
                    <p className={bar.name}>{props.name}</p>
                </div>
            </div>);
            }

export default SideBarItem