import React from 'react';
import s from './Post.module.css';

const Post = (props) => {
    return (
        <div className={s.com}>
            <div className={s.com__img}><img src='https://i.pinimg.com/originals/04/a8/73/04a87347b071ec062a586e02c23f6221.png'/></div>
            <div className={s.com__text}>{props.com}</div>
        </div>
    );
}

export default Post