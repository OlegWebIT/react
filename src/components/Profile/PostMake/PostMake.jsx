import React from 'react';
import main from './PostMake.module.css';


const PostMake = (props) => {

    let newPostElement = React.createRef();
    let addPost = () => {
        let text = newPostElement.current.value;
        props.dispatch({type: 'ADD-POST', text});
        newPostElement.current.value = '';
    }


    return (
        <div>
            <div className={main.post}>
                <div className={main.post__title}>My post</div>
                <div className={main.post__input}><input ref={newPostElement} placeholder='your news...'/></div>
                <div className={main.post__button}>
                    <button onClick={addPost}>Send</button>
                </div>
            </div>
        </div>);
}

export default PostMake