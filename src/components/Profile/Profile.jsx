import React from 'react';
import MyPosts from "./MyPosts/MyPosts";
import ProfileInfo from "./ProfileInfo/ProfileInfo";
import PostMake from "./PostMake/PostMake";

const Profile = (props) => {
    return (
        <div>
            <ProfileInfo/>
            <PostMake dispatch={props.dispatch}/>
            <MyPosts posts={props.state.posts}/>
        </div>);
}

export default Profile