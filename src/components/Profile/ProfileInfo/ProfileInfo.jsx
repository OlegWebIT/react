import React from 'react';
import main from './ProfileInfo.module.css';
import PersonInfo from "./personInfo/personInfo";
import PersonPhoto from "./personPhoto/personPhoto";
import MainPhoto from "./mainPhoto/mainPhoto";


const ProfileInfo = () => {
    return (
        <div className="main__col">
            <MainPhoto />
            <div className={main.main__row}>
                <PersonPhoto />
                <PersonInfo />
            </div>
        </div>


    );
}

export default ProfileInfo