import React from 'react';
import main from './mainPhoto.module.css';


const MainPhoto = () => {
    return (
        <div className="main__col">
            <div className={main.main__img}>
                <img src='https://img5.goodfon.ru/wallpaper/nbig/2/27/zelenyi-fon-tekstura-abstract-background-green-color.jpg'/>
            </div>
        </div>


    );
}

export default MainPhoto