import React from 'react';
import main from './personInfo.module.css';


const PersonInfo = () => {
    return (
        <div className="main__col">
            <div className={main.main__row__text}>
                <div className={main.main__title}>Dmitry K.</div>
                <div className={main.main__subtitle}>
                    <p>Date of birth: 21 February</p>
                    <p>City: Minsk</p>
                    <p>Education: BSU</p>
                    <p>Hobbies: sport</p>
                </div>
            </div>
        </div>);
}

export default PersonInfo