import React from 'react';
import main from './personPhoto.module.css';

const PersonPhoto = () => {
    return (
            <div className={main.main__row__img}>
                <img src='https://i.pinimg.com/originals/04/a8/73/04a87347b071ec062a586e02c23f6221.png'/>
            </div>
    );
}

export default PersonPhoto