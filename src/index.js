import React from 'react';
import './index.css';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import store from './Redux/redux-store'

let renderPage = () => {
    ReactDOM.render(<App state={store.getState()} dispatch={store.dispatch.bind(store)}/>, document.getElementById('root'));
}
renderPage(store.getState());
store.subscribe(() => {
    let state = store.getState();
    renderPage(state);
});


